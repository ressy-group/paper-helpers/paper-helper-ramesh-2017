# Data Gathering from Ramesh 2017

Scripts to merge GenBank-deposited antibody sequences and annotations with
paper-supplied information (see below) into a single CSV table for as many
alleles as possible. See [output/alleles.csv](output/alleles.csv),
[output/alleles.vdj.csv](output/alleles.vdj.csv), and
[output/alleles.vdj.fasta](output/alleles.vdj.fasta) for final output (all else
here is an implementation detail).

The main CSV file contains separate columns for genomic, CDS, and AA sequences
where available. The smaller VDJ CSV and FASTA files contain my own
leader-trimmed (for V) CDS sequences for as many entries as I could manage,
including some V sequences marked non-functional (so long as the genomic
sequence contained recognizable boundaries based on others in the family).  I
also tried to note which look complete or incomplete as some functional entries
are not complete segments and some non-functional entries are.

**NB: The main CSV file is still misssing some information, and shows
small inconsistencies compared to the paper I couldn't resolve here.**  I'll
make updates here, if I find any, to resolve these issues.

Ramesh A, Darko S, Hua A, Overman G, Ransier A, Francica JR, Trama A, Tomaras GD, Haynes BF, Douek DC and Kepler TB (2017) Structure and Diversity of the Rhesus Macaque Immunoglobulin Loci through Multiple De Novo Genome Assemblies. Front. Immunol. 8:1407. doi: [10.3389/fimmu.2017.01407](https://doi.org/10.3389/fimmu.2017.01407)
