from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import csv

wildcard_constraints:
    family="IG[HKL]V[0-9]+",
    acc="[A-Z]{2}[0-9]+"

with open("from-paper/accessions.txt") as f_in:
    ACCESSIONS = [line.strip() for line in f_in]

GBF = expand("from-genbank/{acc}.gbf", acc=ACCESSIONS)
GBFCSV = expand("converted/{acc}.gbf.csv", acc=ACCESSIONS)

SHEETS_URL = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQC9Yh2l3ktopxK0idgGlSaOeo2Chnq15EoVro3wsmKHowVP1uVydyVJ_asCQe9Sfwot7_PcTNzaKGa/pub"
SHEETS_GIDS = {
    "fig1": "1857954478",
    "fig2": "380706608",
    "fig3": "1042301659",
    "fig4": "1508567143",
    "fig5": "782424017",
    "fig6": "1329373153",
    "suppsheet1": "974273021",
    "suppsheet2": "1621508193",
    "suppsheet3": "664446886",
    "table1": "655585230",
    "table2": "958959819",
    "table3": "78785043",
    "table4": "1988184408"
    }
SHEETS = expand("from-paper/{sheet}.csv", sheet=SHEETS_GIDS.keys())

rule all:
    input: ["output/alleles.csv", "output/alleles.vdj.csv", "output/alleles.vdj.fasta"]

rule merge_everything:
    output: "output/alleles.csv"
    input:
        from_gb="converted/all.csv",
        from_paper=SHEETS
    shell: "Rscript scripts/merge_everything.R"

rule convert_gbf_csv_combined:
    """Merge the per-accession CSV files with per-feature rows into one CSV."""
    output: "converted/all.csv"
    input: GBFCSV
    run:
        import csv
        rows = []
        csv.field_size_limit(10000000)
        for fp in input:
            with open(fp) as f_in:
                reader = csv.DictReader(f_in)
                rows.extend(list(reader))
        with open(output[0], "wt") as f_out:
            writer = csv.DictWriter(f_out, fieldnames=rows[0].keys(), lineterminator="\n")
            writer.writeheader()
            writer.writerows(rows)

rule all_gbf_csv:
    input: GBFCSV

rule all_download_gbf:
    input: GBF

rule all_sheets:
    input: SHEETS

rule convert_gbf_csv:
    """Convert a GBF file into a CSV with one row per feature."""
    output: "converted/{acc}.gbf.csv"
    input: "from-genbank/{acc}.gbf"
    shell: "python scripts/convert_gbf.py {input} {output}"

rule download_gbf:
    """Download one GBF text file per GenBank accession.

    We can then convert the GBF into other formats like individual feature
    sequences in FASTA.
    """
    output: "from-genbank/{acc}.gbf"
    shell: "python scripts/download_genbank.py {wildcards.acc} gb > {output}"

rule download_sheet:
    output: "from-paper/{sheet}.csv"
    params:
        url=SHEETS_URL,
        gid=lambda w: SHEETS_GIDS[w.sheet]
    shell:
        """
            curl -L '{params.url}?gid={params.gid}&single=true&output=csv' > {output}
            dos2unix {output}
            echo >> {output}
        """

###### Manually-guided alignments and trimming

FAMILIES = [
    "IGHV1",
    "IGHV2",
    "IGHV3",
    "IGHV4",
    "IGHV5",
    "IGHV6",
    "IGHV7",
    "IGKV1",
    "IGKV2",
    "IGKV3",
    "IGKV4",
    "IGKV5",
    "IGKV6",
    "IGKV7",
    "IGLV1",
    "IGLV2",
    "IGLV3",
    "IGLV4",
    "IGLV5",
    "IGLV6",
    "IGLV7",
    "IGLV8",
    "IGLV9",
    "IGLV10",
    "IGLV11"]

rule alleles_vdj_fasta:
    output: "output/alleles.vdj.fasta"
    input: "output/alleles.vdj.csv"
    run:
        with open(input[0]) as f_in, open(output[0], "wt") as f_out:
            reader = csv.DictReader(f_in)
            for row in reader:
                if not row["VDJSeq"]:
                    continue
                # Dataset,Category,VStatus
                fields = {k: row[k] for k in ["Dataset", "Category", "VStatus"]}
                fields = [f"{k}={v}" for k, v in fields.items() if v]
                fields = " ".join(fields)
                SeqIO.write(SeqRecord(
                    Seq(row["VDJSeq"]),
                    id=row["Allele"],
                    description=fields), f_out, "fasta-2line")

# Make a smaller, tidied version of the main alleles table, with just V(D)J
# genes, trimmed where needed
rule annotate_table:
    output: "output/alleles.vdj.csv"
    input:
        original="output/alleles.csv",
        annotations="analysis/annotated.csv"
    run:
        orig = {}
        with open(input.original) as f_in:
            reader = csv.DictReader(f_in)
            for row in reader:
                if row["Region"] != "constant":
                    orig[row["Allele"]] = row
        annots = {}
        with open(input.annotations) as f_in:
            reader = csv.DictReader(f_in)
            for row in reader:
                annots[row["SeqID"]] = row
        with open(output[0], "wt") as f_out:
            writer = csv.DictWriter(
                f_out, fieldnames=["Allele", "Dataset", "Category", "VStatus", "VDJSeq"],
                lineterminator="\n")
            writer.writeheader()
            for seqid in orig:
                row = {key: orig[seqid][key] for key in ["Allele", "Dataset", "Category"]}
                if seqid in annots:
                    row["VStatus"] = re.sub("Nonfunctional|Functional", "", annots[seqid]["VDJGroup"])
                    row["VDJSeq"] = annots[seqid]["VDJSeq"]
                else:
                    row["VDJSeq"] = orig[seqid]["SeqCDS"]
                writer.writerow(row)

# Combine the per-family annotation tables
# To help check things, using SONAR's offshoot of the Ramesh sequences from:
# https://github.com/scharch/SONAR/blob/master/germDB/IgHKLV_BU_DD.fasta
# (All the SONAR entries should be here, if not the other way around, after
# accounting for some slight differences in naming.)
rule all_annotate:
    output: "analysis/annotated.csv"
    input:
        byfamily=expand("analysis/{family}/annotated.csv", family=FAMILIES),
        original="paper-helper-ramesh-2017/output/alleles.csv",
        sonar="SONAR_IgHKLV_BU_DD.fasta"
    run:
        orig = {}
        with open(input.original) as f_in:
            reader = csv.DictReader(f_in)
            for row in reader:
                if row["Region"] == "variable":
                    orig[row["Allele"]] = row["SeqGenomic"]

        sonar = {}
        with open(input.sonar) as f_in:
            for record in SeqIO.parse(f_in, "fasta"):
                seqid = re.sub(r"-[XUS]\*", "*", re.sub("^ORF_", "", record.id))
                sonar[seqid] = str(record.seq)

        rows = []
        for csv_path in input.byfamily:
            with open(csv_path) as f_in:
                reader = csv.DictReader(f_in)
                rows.extend(list(reader))
        check = {row["SeqID"]: row["VDJSeq"] for row in rows}

        if not all(key in orig.keys() for key in check.keys()):
            extra = set(check.keys()) - set(orig.keys())
            raise ValueError(f"Unexpected sequence IDs: {extra}")
        if not all(key in check.keys() for key in orig.keys()):
            missing = set(orig.keys()) - set(check.keys())
            raise ValueError(f"Missing sequence IDs: {missing}")
        for key in orig:
            if check[key] not in orig[key]:
                raise ValueError(f"Sequence content mismatch for: {key}")

        if not all(key in check.keys() for key in sonar):
            missing = set(sonar.keys()) - set(check.keys())
            raise ValueError(f"Missing sequence ID from SONAR: {missing}")
        for key in sonar:
            if check[key] not in sonar[key]:
                raise ValueError(f"SONAR sequence content mismatch for: {key}")

        with open(output[0], "wt") as f_out:
            writer = csv.DictWriter(f_out, fieldnames=rows[0].keys(), lineterminator="\n")
            writer.writeheader()
            writer.writerows(rows)

GROUPS = ["FunctionalComplete", "NonfunctionalComplete", "FunctionalIncomplete", "NonfunctionalIncomplete", "NonfunctionalOther"]
# Aggregate the set of manually-prepped alignments for a given family into one
# table of ungapped sequences, and do some sanity-checking to make sure all
# sequences are accounted for
rule annotate:
    output: "analysis/{family}/annotated.csv"
    input:
        annotated=expand("analysis/{{family}}/{group}.fasta", group=GROUPS),
        original="analysis/full.{family}.aln.sorted.fasta"
    run:
        orig = {}
        with open(input.original) as f_in:
            for record in SeqIO.parse(f_in, "fasta"):
                orig[record.id] = re.sub("-", "", str(record.seq))
        rows = []
        for fasta in input.annotated:
            group = Path(fasta).stem
            with open(fasta) as f_in:
                for record in SeqIO.parse(f_in, "fasta"):
                    rows.append({
                        "SeqID": record.id,
                        "VDJGroup": group,
                        "VDJSeq": re.sub("-", "", str(record.seq)),
                        })
        check = {row["SeqID"]: row["VDJSeq"] for row in rows}
        # we should have the same seqs as went in, just split up and trimmed
        if not all(key in orig.keys() for key in check.keys()):
            extra = set(check.keys()) - set(orig.keys())
            raise ValueError(f"Unexpected sequence IDs: {extra}")
        if not all(key in check.keys() for key in orig.keys()):
            missing = set(orig.keys()) - set(check.keys())
            raise ValueError(f"Missing sequence IDs: {missing}")
        for key in orig:
            if check[key] not in orig[key]:
                raise ValueError(f"Sequence content mismatch for: {key}")
        # empty out the NonfunctionalOther seqs since those aren't actually
        # trimmed (since I don't know what the boundaries on those are)
        for row in rows:
            if row["VDJGroup"] == "NonfunctionalOther":
                row["VDJSeq"] = ""
        with open(output[0], "wt") as f_out:
            writer = csv.DictWriter(f_out, fieldnames=rows[0].keys(), lineterminator="\n")
            writer.writeheader()
            writer.writerows(rows)

# sort aligned seqs in original order
rule sort_alignment:
    output: "analysis/{prefix}.aln.sorted.fasta"
    input:
        aln="analysis/{prefix}.aln.fasta",
        orig="analysis/{prefix}.fasta"
    run:
        with open(input.orig) as f_in:
            ids = [rec.id for rec in SeqIO.parse(f_in, "fasta")]
        with open(input.aln) as f_in:
            records = SeqIO.to_dict(SeqIO.parse(f_in, "fasta"))
        with open(output[0], "wt") as f_out:
            for seqid in ids:
                SeqIO.write(records[seqid], f_out, "fasta-2line")

# align all the seqs from one family
rule align_family:
    output: "analysis/{prefix}.{family}.aln.fasta"
    input: "analysis/{prefix}.{family}.fasta"
    shell:
        """
            if [ -s {input} ]; then
                muscle -quiet -in {input} -out {output}
            else
                touch {output}
            fi
        """

# gather CDS, or closest we have, for one gene family
rule get_full_set:
    output: "analysis/full.{family}.fasta"
    input: "output/alleles.csv"
    run:
        with open(input[0]) as f_in, open(output[0], "wt") as f_out:
            reader = csv.DictReader(f_in)
            records = []
            for row in reader:
                if row["Family"] == wildcards.family:
                    if row["Category"] == "Functional":
                        seqcol = "SeqCDS"
                    else:
                        seqcol = "SeqGenomic"
                    record = SeqRecord(
                        Seq(row[seqcol]),
                        id=row["Allele"],
                        description=f"Category={row['Category']} Dataset={row['Dataset']} SeqCol={seqcol}")
                    record.annotations["Dataset"] = row["Dataset"]
                    record.annotations["Category"] = row["Category"]
                    records.append(record)
            records = sorted(records, key=lambda rec: (
                rec.annotations["Dataset"],
                rec.annotations["Category"],
                -len(rec)))
            SeqIO.write(records, f_out, "fasta-2line")
